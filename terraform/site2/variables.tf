variable "prefix" {
  default = "<>"
}

variable "region" {
  default = "<>"
}

variable "ssh_public_key_file" {
  default = "../../../../keys/id_rsa.pub"
}

variable "external_ip_allocation" {
  default = "<>"
}

variable "geo_site" {
  default = "<>"
}

variable "geo_deployment" {
  default = "<>"
}
