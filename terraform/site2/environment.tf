module "gitlab_ref_arch_aws" {
  source = "../../../modules/gitlab_ref_arch_aws"

  prefix         = var.prefix
  ssh_public_key = file(var.ssh_public_key_file)

  geo_site       = var.geo_site
  geo_deployment = var.geo_deployment

  # 1k

  gitlab_rails_node_count    = 1
  gitlab_rails_instance_type = "c5.2xlarge"

  haproxy_external_node_count                = 1
  haproxy_external_instance_type             = "c5n.xlarge"
  haproxy_external_elastic_ip_allocation_ids = [var.external_ip_allocation]


  # redis configuration

  elasticache_redis_instance_type                  = "m5.xlarge"
  elasticache_redis_node_count                     = 1
  elasticache_redis_password                       = "<>"

  # RDS configuration

  rds_postgres_instance_type                       = "m5.xlarge"
  rds_postgres_password                            = "<>"
  rds_postgres_iam_database_authentication_enabled = false
  rds_postgres_kms_key_arn                         = "<>"
  rds_postgres_replication_database_arn            = "<>"

  rds_geo_tracking_postgres_instance_type                       = "m5.large"
  rds_geo_tracking_postgres_password                            = "<>"
  rds_geo_tracking_postgres_iam_database_authentication_enabled = false
}

output "gitlab_ref_arch_aws" {
  value = module.gitlab_ref_arch_aws
}