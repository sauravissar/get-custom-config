terraform {
  backend "s3" {
    bucket = "<>"
    key    = "<>"
    region = "<>"
  }
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = var.region
}
