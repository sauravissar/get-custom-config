module "gitlab_ref_arch_aws" {
  source = "../../../modules/gitlab_ref_arch_aws"

  prefix         = var.prefix
  ssh_public_key = file(var.ssh_public_key_file)

  geo_site       = var.geo_site
  geo_deployment = var.geo_deployment

  # 3k

  gitaly_node_count    = 3
  gitaly_instance_type = "m5.xlarge"

  praefect_node_count    = 3
  praefect_instance_type = "c5.xlarge"

  gitlab_rails_node_count    = 3
  gitlab_rails_instance_type = "c5.xlarge"

  haproxy_external_node_count                = 1
  haproxy_external_instance_type             = "c5n.xlarge"
  haproxy_external_elastic_ip_allocation_ids = [var.external_ip_allocation]
  elb_internal_create                        = true

  monitor_node_count    = 1
  monitor_instance_type = "c5.xlarge"


  sidekiq_node_count    = 4
  sidekiq_instance_type = "m5.xlarge"

  # RDS configuration

  rds_postgres_instance_type                                = "m5.xlarge"
  rds_postgres_password                                     = "<>"
  rds_postgres_backup_retention_period                      = 14
  rds_postgres_iam_database_authentication_enabled          = false
  rds_postgres_kms_key_arn                                  = "<>"

  rds_praefect_postgres_instance_type                       = "m5.xlarge"
  rds_praefect_postgres_password                            = "<>"
  rds_praefect_postgres_iam_database_authentication_enabled = false

  elasticache_redis_instance_type                           = "m5.2xlarge"
  elasticache_redis_node_count                              = 2
  elasticache_redis_password                                = "<>"

}

output "gitlab_ref_arch_aws" {
  value = module.gitlab_ref_arch_aws
}
