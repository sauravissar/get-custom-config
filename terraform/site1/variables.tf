variable "prefix" {
  default = "dxb"
}

variable "region" {
  default = "us-east-1"
}

variable "ssh_public_key_file" {
  default = "../../../../keys/id_rsa.pub"
}

variable "external_ip_allocation" {
  default = ""
}

variable "geo_site" {
  default = "<>"
}

variable "geo_deployment" {
  default = "<>"
}
